var proto = Object.create(HTMLElement.prototype, {
    config: {
        get: function () {
            return this._config;
        },
        set: function (config) {
            this._config = config;
            if (this._editor !== null) {
                this.detachedCallback();
                this.attachedCallback();
            }
        }
    },
    content: {
        get: function () {
            return this._content;
        },
        set: function (content) {
            if (content !== this._content) {
                this._content = content;
                if (this._editor !== null) {
                    this._editor.setData(this._content);
                }
            }
        }
    },
    createdCallback: {
        value: function () {
            this._config = {};
            this._content = '';
            this._editor = null;
            this._contentUpdate = null;
        }
    },
    attachedCallback: {
        value: function () {
            this._editor = CKEDITOR.appendTo(
                this, this._config, this._content);
            var base = this;
            base._editor.on('change', function (e) {
                var content = base._editor.getData();
                base._content = content;
                var event = new CustomEvent(
                    'ckeditorchange',
                    {detail: content});
                base.dispatchEvent(event);
            });
        }
    },
    detachedCallback: {
        value: function () {
            this._editor.destroy();
            this._editor = null;
        }
    }
});
document.registerElement('x-ckeditor', {prototype: proto});

# CKEditor Web Component

```
yarn add webcomponent-ckeditor
```

This library provides a custom HTML element `x-ckeditor` which wraps the CKEditor as a web component.

It was primarily developed to be used with [Elm](http://elm-lang.org/), but should also work fine with other frameworks like React or with vanilla JS.

Be sure to check browser support and include a polyfill if necessary. See https://www.webcomponents.org/ for details.

This library uses v0 of the custom elements spec, because the official polyfill doesn't support v1 yet at the time of writing.

## Properties

| Property  | Type     | Description                       |
| --------- | -------- | --------------------------------- |
| `config`  | `object` | CKEditor config                   |
| `content` | `string` | HTML string of the editor content |

## Events

| Event            | Type               | Description                                                                          |
| ---------------- | ------------------ | ------------------------------------------------------------------------------------ |
| `ckeditorchange` | `{detail: string}` | Fired when the CKEditor content changes, `event.detail` contains the new HTML string |